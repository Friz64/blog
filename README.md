# Source repository for [blog.friz64.de](https://blog.friz64.de/)

I use the [Zola SSG](https://www.getzola.org/) with my own modified version
of the [zerm](https://github.com/ejmg/zerm) theme.

All the articles are contained within the `content/` directory.

## Licensing

The `zerm` theme, which this blog's theme is based on, is licensed under
[`The MIT License (MIT)`](LICENSE-ZERM).

The `content/` directory is licensed under [`CC BY-NC-ND 4.0`](LICENSE-CONTENT).

Furthermore, all the code in the `content/` directory, that is in a markdown code block
is additionally licensed under the [`zlib License`](LICENSE-CONTENT-CODE).
