+++
title = "about"
path = "about"
+++

Hi! I'm known under the online nickname of _Friz64_ (you can also just refer to me as _Friz_).
My preferred pronouns are he/him.

I discovered the [Rust programming language](https://www.rust-lang.org/) in 2018. My project
folder has grown well over the size of 200 items since then, reaching from mostly failed
experiments to various fully fledged programs. This blog is all about this hobby of mine.

Currently, I am maintaining the Rust crate [`erupt`](https://docs.rs/erupt/),
which aims to provide ergonomic Vulkan API bindings while focusing on broad auto-generation.

Check out my open source projects on [GitLab](https://gitlab.com/Friz64)
or [GitHub](https://github.com/Friz64).

# Contact

- Discord: `Friz64#4876`
- Email: `friz64@protonmail.com`
- Twitter: [`@Friz64_`](https://twitter.com/Friz64_)
